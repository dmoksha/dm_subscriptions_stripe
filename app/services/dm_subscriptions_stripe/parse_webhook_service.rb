# frozen_string_literal: true

#------------------------------------------------------------------------------
module DmSubscriptionsStripe
  class ParseWebhookService
    include DmCore::ServiceSupport

    def initialize(request:, secret:)
      @request = request
      @secret  = secret
    end

    def call
      payload = request.body.read

      if secret.present?
        # Retrieve the event by verifying the signature using the raw body and secret if webhook signing is configured.
        sig_header = request.env['HTTP_STRIPE_SIGNATURE']

        begin
          ::Stripe::Webhook.construct_event(payload, sig_header, secret)
        rescue JSON::ParserError => e
          log_exception('Payload from Stripe was invalid', e)
          raise
        rescue Stripe::SignatureVerificationError => e
          log_exception('Webhook signature verification failed', e)
          raise
        end
      else
        data = JSON.parse(payload, symbolize_names: true)

        ::Stripe::Event.construct_from(data)
      end
    end

    private

    attr_reader :request, :secret

    def log_exception(msg, event)
      Rails.logger.error("\nStripe: #{msg}: \n#{event.message}\n\t#{event.backtrace.join("\n\t")}\n")
    end
  end
end
