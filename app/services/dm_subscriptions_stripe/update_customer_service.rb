# frozen_string_literal: true

# this service wraps PaymentApiStripeSubscription#update_customer
#------------------------------------------------------------------------------
module DmSubscriptionsStripe
  class UpdateCustomerService
    include DmCore::ServiceSupport

    def initialize(customer_id:, options: {})
      @customer_id = customer_id
      @options     = options
    end

    def call
      return unless customer_id
      return if options.empty?

      api = DmSubscriptionsStripe::PaymentApiStripeSubscription.new

      api.update_customer(customer_id: customer_id, options: options)
    end

    private

    attr_reader :customer_id, :options
  end
end
