# frozen_string_literal: true

module DmSubscriptionsStripe
  class UpdatePaymentMethodService
    include DmCore::ServiceSupport

    def initialize(current_user:, payment_method_token:, subscription_plan:)
      @current_user         = current_user
      @payment_method_token = payment_method_token
      @subscription_plan    = subscription_plan
    end

    def call
      DmSubscriptionsStripe::PaymentApiStripeSubscription.new

      raise StandardError, 'NOT IMPLEMENTED YET'

      # customer = api.create_customer(
      #   user: current_user,
      #   payment_method_token: payment_method_token,
      #   options: { description: "Plan: #{subscription_plan.name}" }
      # )
      #
      # return unless customer
      #
      # current_user.current_site_profile.update_attribute(:stripe_customer_id, customer.id)
      #
      # # this will create the customers subscription.  Since the payment method
      # # has been set, then it will attempt to charge the card.
      # api.create_subscription_to_plan(customer.id, subscription_plan.stripe_plan_id)
    end

    private

    attr_reader :current_user, :payment_method_token, :subscription_plan
  end
end
