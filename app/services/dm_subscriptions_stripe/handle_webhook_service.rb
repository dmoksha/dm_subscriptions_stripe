# frozen_string_literal: true

#------------------------------------------------------------------------------
module DmSubscriptionsStripe
  class HandleWebhookService
    include DmCore::ServiceSupport

    #------------------------------------------------------------------------------
    def initialize(event)
      @event = event
    end

    #------------------------------------------------------------------------------
    def call
      # Get the type of webhook event sent - used to check the status of PaymentIntents.
      event_type  = event['type']
      data        = event['data']
      data_object = data['object']

      case event_type
      when 'customer.created'
        customer_created(data_object)
      when 'customer.updated'
        customer_updated(data_object)
      when 'invoice.created'
        invoice_created(data_object)
      when 'invoice.payment_succeeded'
        invoice_payment_succeeded(data_object)
      when 'invoice.payment_failed'
        invoice_payment_failed(data_object)
      when 'customer.subscription.created'
        customer_subscription_created(data_object)
      else
        log_unknown_event(event_type, data_object)
      end
    end

    private

    attr_reader :event

    #------------------------------------------------------------------------------
    def customer_created(data_object)
      log_event('customer_created', data_object)
    end

    #------------------------------------------------------------------------------
    def customer_updated(data_object)
      log_event('customer_updated', data_object)
    end

    #------------------------------------------------------------------------------
    def invoice_created(data_object)
      log_event('invoice_created', data_object)
    end

    #------------------------------------------------------------------------------
    def invoice_payment_succeeded(data_object)
      log_event('invoice_payment_succeeded', data_object)
    end

    #------------------------------------------------------------------------------
    def invoice_payment_failed(data_object)
      log_event('invoice_payment_failed', data_object)
    end

    #------------------------------------------------------------------------------
    def customer_subscription_created(data_object)
      log_event('customer_subscription_created', data_object)
    end

    #------------------------------------------------------------------------------
    def log_exception(msg, event)
      Rails.logger.error("\nStripe: #{msg}: \n#{event.message}\n\t#{event.backtrace.join("\n\t")}\n")
    end

    #------------------------------------------------------------------------------
    def log_unknown_event(event_type, data_object)
      Rails.logger.error("StripeBillingController: unknown webhook msg `#{event_type}`")
      Rails.logger.error("\n  data_object: #{data_object}")
    end

    #------------------------------------------------------------------------------
    def log_event(event_type, data_object)
      Rails.logger.error("StripeBillingController: webhook msg `#{event_type}`")
      Rails.logger.error("\n  data_object: #{data_object}")
    end
  end
end
