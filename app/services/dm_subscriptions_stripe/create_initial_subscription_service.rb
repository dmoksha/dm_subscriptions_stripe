# frozen_string_literal: true

module DmSubscriptionsStripe
  class CreateInitialSubscriptionService
    include DmCore::ServiceSupport

    class NoStripeSubscriptionPlan < StandardError; end

    def initialize(current_user:, payment_method_token:, subscription_plan:)
      @current_user         = current_user
      @customer_id          = current_user&.current_site_profile&.stripe_customer_id
      @payment_method_token = payment_method_token
      @subscription_plan    = subscription_plan
      @api                  = DmSubscriptionsStripe::PaymentApiStripeSubscription.new
    end

    def call
      return unless current_user
      return unless customer_id
      raise NoStripeSubscriptionPlan, 'No Stripe subscription plan associated with this plan' unless subscription_plan.stripe_plan_id

      # this will create the customers subscription.  Since the payment method
      # has been set, then it will attempt to charge the card.
      api.create_subscription_to_plan(customer_id, subscription_plan.stripe_plan_id)
    end

    private

    attr_reader :current_user, :payment_method_token, :subscription_plan, :api, :customer_id
  end
end
