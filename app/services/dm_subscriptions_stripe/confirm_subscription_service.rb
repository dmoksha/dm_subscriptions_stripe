# frozen_string_literal: true

# Given a subscription from Stripe, confirm it in our system
#------------------------------------------------------------------------------
module DmSubscriptionsStripe
  class ConfirmSubscriptionService
    include DmCore::ServiceSupport

    def initialize(current_user:, subscription:)
      @current_user = current_user
      @subscription = subscription
    end

    def call
      return unless current_user
      return unless subscription

      false
    end

    private

    attr_reader :current_user, :subscription

    def current_customer_id
      @current_customer_id ||= current_user.current_site_profile.stripe_customer_id
    end
  end
end
