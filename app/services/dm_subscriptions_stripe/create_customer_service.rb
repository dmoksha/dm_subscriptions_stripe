# frozen_string_literal: true

# this service wraps PaymentApiStripeSubscription#create_customer
#------------------------------------------------------------------------------
module DmSubscriptionsStripe
  class CreateCustomerService
    include DmCore::ServiceSupport

    def initialize(current_user:, payment_method_token:)
      @current_user         = current_user
      @payment_method_token = payment_method_token
    end

    def call
      return unless current_user
      return current_customer_id if current_customer_id
      return unless payment_method_token

      api = DmSubscriptionsStripe::PaymentApiStripeSubscription.new

      customer = api.create_customer(
        user: current_user,
        payment_method_token: payment_method_token,
        options: { description: current_user.full_name }
      )

      return unless customer

      current_user.current_site_profile.update_attribute(:stripe_customer_id, customer.id)

      customer
    end

    private

    attr_reader :current_user, :payment_method_token

    def current_customer_id
      @current_customer_id ||= current_user.current_site_profile.stripe_customer_id
    end
  end
end
