# frozen_string_literal: true

module DmSubscriptionsStripe
  class AdminMenuInject
    #------------------------------------------------------------------------------
    def self.menu_items(user)
      menu = []
      menu << { text: 'Subscriptions', icon_class: :subscriptions, link: DmSubscriptionsStripe::Engine.routes.url_helpers.admin_subscription_plans_path(locale: I18n.locale) } if user.can?(:manage_subscriptions, :all)
      menu
    end
  end
end
