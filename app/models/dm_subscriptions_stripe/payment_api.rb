# frozen_string_literal: true

# Abstract out access to various processor specific helper functions.
#------------------------------------------------------------------------------
class DmSubscriptionsStripe::PaymentApi
  def self.api
    case Account.current.preferred_subscription_processor
    when 'stripe_subscription'
      DmSubscriptionsStripe::PaymentApiStripeSubscription.new
    else
      DmSubscriptionsStripe::PaymentApiBogus.new
    end
  end

  #------------------------------------------------------------------------------
  def gateway
    nil
  end

  #------------------------------------------------------------------------------
  def processor
    ''
  end

  #------------------------------------------------------------------------------
  def stripe?
    false
  end

  # indicates if this gateway uses a javascript bridge
  #------------------------------------------------------------------------------
  def javascript_bridge?
    false
  end
end
