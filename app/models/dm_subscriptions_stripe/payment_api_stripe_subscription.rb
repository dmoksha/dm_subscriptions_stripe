# frozen_string_literal: true

module DmSubscriptionsStripe
  # Interacts directly with the Stripe API to support their Billing/Subscription
  # functions.  Does not interface with ActiveMerchant
  #------------------------------------------------------------------------------
  class PaymentApiStripeSubscription < DmSubscriptionsStripe::PaymentApi
    # Stripe api supports passing in the api key to each call.  Pull the key from
    # the Account
    #------------------------------------------------------------------------------
    def api_key
      Rails.application.secrets[Account.current.account_prefix]['stripe_private_key']
    end

    #------------------------------------------------------------------------------
    def public_key
      Rails.application.secrets[Account.current.account_prefix]['stripe_public_key']
    end

    #------------------------------------------------------------------------------
    def processor
      'stripe_subscription'
    end

    #------------------------------------------------------------------------------
    def stripe?
      true
    end

    # indicates if this gateway uses a javascript bridge
    #------------------------------------------------------------------------------
    def javascript_bridge?
      true
    end

    #------------------------------------------------------------------------------
    def gateway
      nil
    end

    #------------------------------------------------------------------------------
    def find_customer(customer_token)
      Stripe::Customer.retrieve(customer_token, api_key)
    end

    # Creates a new Customer and attaches the PaymentMethod in one API call
    #------------------------------------------------------------------------------
    def create_customer(user:, payment_method_token:, options: {})
      params = {
        email: user.email,
        name: user.full_name,
        metadata: { user_id: user.id },
        payment_method: payment_method_token,
        invoice_settings: { default_payment_method: payment_method_token }
      }

      params[:description] = options[:description] unless options[:description].blank?

      Stripe::Customer.create(params, api_key: api_key)
    rescue Stripe::StripeError => e
      handle_exception(e)
      nil
    end

    # Creates a new Customer and attaches the PaymentMethod in one API call
    #------------------------------------------------------------------------------
    def update_customer(customer_id:, options: {})
      params = {}

      params[:invoice_settings] = { default_payment_method: options[:invoice_payment] } if options[:invoice_payment]

      params[:description] = options[:description] if options[:description]

      Stripe::Customer.update(customer_id, params, api_key: api_key)
    rescue Stripe::StripeError => e
      handle_exception(e)
      nil
    end

    #------------------------------------------------------------------------------
    def attach_payment_method(customer_id:, payment_method_token:, make_default: true)
      return unless customer_id
      return unless payment_method_token

      Stripe::PaymentMethod.attach(payment_method_token, { customer: customer_id }, api_key: api_key)

      update_customer(customer_id: customer_id, options: { invoice_payment: payment_method_token }) if make_default
    rescue Stripe::StripeError => e
      handle_exception(e)
      nil
    end

    def create_subscription_to_plan(customer_id, plan_id)
      params = {
        customer: customer_id,
        items: [{ plan: plan_id }],
        expand: ['latest_invoice.payment_intent']
      }

      Stripe::Subscription.create(params, api_key: api_key)
    rescue Stripe::StripeError => e
      handle_exception(e)
      nil
    end

    private

    def handle_exception(event)
      case event
      when Stripe::CardError
        log_exception('Card error', event)
        Rails.logger.error("\n  Status is: #{event.http_status}")
        Rails.logger.error("\n  Type is: #{event.error.type}")
        Rails.logger.error("\n  Charge ID is: #{event.error.charge}")
        Rails.logger.error("\n  Code is: #{event.error.code}") if event.error.code
        Rails.logger.error("\n  Decline code is: #{event.error.decline_code}") if event.error.decline_code
        Rails.logger.error("\n  Param is: #{event.error.param}") if event.error.param
      when Stripe::RateLimitError
        log_exception('Too many requests made to the API too quickly', event)
      when Stripe::InvalidRequestError
        log_exception('Invalid parameters were supplied to Stripe API', event)
      when Stripe::AuthenticationError
        log_exception('Authentication with Stripe API failed', event)
      when Stripe::APIConnectionError
        log_exception('Network communication with Stripe failed', event)
      when Stripe::StripeError
        log_exception('Generic error', event)
      else
        # Something else happened, completely unrelated to Stripe
        raise event
      end
    end

    def log_exception(msg, event)
      Rails.logger.error("\nStripe: #{msg}: \n#{event.message}\n\t#{event.backtrace.join("\n\t")}\n")
    end

    # module Subscription
    #
    #   # create a new subscription to the specified plan, and return the subscription id
    #   #------------------------------------------------------------------------------
    #   def self.create(options = { plan: '', customer_token: '', card_token: '' })
    #     customer      = Customer.find(options[:customer_token])
    #     card          = customer.cards.create(card: options[:card_token])
    #     subscription  = customer.update_subscription(plan: options[:plan])
    #     subscription.id
    #   end
    # end

    # # subscription plans
    # #------------------------------------------------------------------------------
    # def self.plans(options = {})
    #   result = Stripe::Plan.all({}, api_key)
    #   plans = []
    #   result[:data].each do |plan|
    #     plans << {vendor_plan_id: plan[:id], name: plan[:name], amount_cents: plan[:amount], currency: plan[:currency],
    #               interval: plan[:interval], trial_period_days: plan[:trial_period_days] }
    #   end
    #   return plans
    # end
  end
end
