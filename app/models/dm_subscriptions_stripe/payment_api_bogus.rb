# frozen_string_literal: true

# Abstract out access to various processor specific
# helper functions for a bogus payment method (for testing)
#------------------------------------------------------------------------------
class DmSubscriptionsStripe::PaymentApiBogus < DmSubscriptionsStripe::PaymentApi
  #------------------------------------------------------------------------------
  def api_key
    ''
  end

  #------------------------------------------------------------------------------
  def public_key
    ''
  end

  #------------------------------------------------------------------------------
  def processor
    'bogus'
  end

  #------------------------------------------------------------------------------
  def gateway
    @gateway ||= ActiveMerchant::Billing::Base.gateway(:bogus).new
  end
end
