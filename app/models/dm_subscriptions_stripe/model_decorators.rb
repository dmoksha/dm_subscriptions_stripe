# frozen_string_literal: true

# Decorate any models
# Chose to use this method for now, instead of creating a decorator folder and
# eager-loading it.  Prefer to keep the logic in the model folder.
#------------------------------------------------------------------------------
module DmSubscriptionsStripe
  module ModelDecorators
    Ability.include DmSubscriptionsStripe::Concerns::Ability
    User.include DmSubscriptionsStripe::Concerns::User
  end
end
