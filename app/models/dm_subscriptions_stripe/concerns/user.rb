# frozen_string_literal: true

module DmSubscriptionsStripe
  module Concerns
    module User
      extend ActiveSupport::Concern

      included do
        # has_many    :user_subscriptions,    class_name: "DmSubscriptionsStripe::UserSubscription"
        # has_one     :current_subscription,  -> { where(account_id: Account.current.id) }, class_name: 'DmSubscriptionsStripe::UserSubscription'
        # has_subscription
      end

      module ClassMethods
      end
    end
  end
end
