# frozen_string_literal: true

module DmSubscriptionsStripe
  module Concerns
    module ActsAsSubscription
      extend ActiveSupport::Concern

      included do
      end

      module ClassMethods
        # The limits hash is used to add some methods to the class that you can
        # use to check whether the subscriber has the rights to do something.
        # The hash keys match the *_limit fields in the subscriptions and
        # subscription_plan tables, and the values are the methods that will be
        # called to see if that limit has been reached.  For example,
        #
        # { 'user_limit' => Proc.new {|a| a.users.count } }
        #
        # defines a single limit based on the user_limit attribute that would
        # call users.account on the instance of the model that is invoking this
        # method.  In other words, if you have this:
        #
        # class Account < ApplicationRecord
        #   has_subscription( limits: { 'user_limit' => Proc.new {|a| a.users.count } })
        # end
        #
        # then you could call @account.reached_user_limit? to know whether to allow
        # the account to create another user.
        #------------------------------------------------------------------------------
        def has_subscription(options = { limits: {} })
          has_one         :subscription,          as: :subscriber, dependent: :destroy
          has_many        :subscription_payments, as: :subscriber

          accepts_nested_attributes_for :subscription

          validate        :valid_plan?,         on: :create
          validate        :valid_subscription?, on: :create

          send(:include, HasSubscriptionInstanceMethods)

          attr_accessor   :plan, :plan_start, :creditcard, :address

          class_attribute :subscription_limits

          self.subscription_limits = options[:limits] || {}

          # Create methods for each limit for checking the limit.
          # E.g., for a field user_limit, this will define reached_user_limit?
          subscription_limits.each do |name, meth|
            define_method("reached_#{name}?") do
              return false unless subscription

              # A nil value will always return false, or, in other words, nil == unlimited
              subscription.send(name) && subscription.send(name) <= meth.call(self)
            end
          end
        end
      end

      module HasSubscriptionInstanceMethods
        # Does the account qualify for a particular subscription plan
        # based on the plan's limits
        #------------------------------------------------------------------------------
        def qualifies_for?(plan)
          self.class.subscription_limits.map do |name, meth|
            limit = plan.send(name)
            !limit || (meth.call(self) <= limit)
          end.all?
        end

        # [todo]
        #------------------------------------------------------------------------------
        def needs_payment_info?
          if new_record?
            DmSubscriptionsStripe.config.require_payment_info_for_trials && @plan && (@plan.amount + @plan.setup_amount).positive?
          else
            subscription&.needs_payment_info?
          end
        end

        # is the user allowed to access the subscribed content?  either it's a trial
        # subscription or a currently paid suscription
        #------------------------------------------------------------------------------
        def subscribed_content_allowed?
          return false unless subscription

          subscription.current? && ((subscription.active? && subscription.currently_paid?) || subscription.trial?)
        end

        protected

        # It's ok if a plan is not specified
        #------------------------------------------------------------------------------
        def valid_plan?
          if @plan
            @selected_plan = SubscriptionPlan.friendly.find(@plan)
            errors.add(:base, 'Invalid plan selected.') unless @selected_plan
          end
        end

        #------------------------------------------------------------------------------
        def valid_subscription?
          return if errors.any? # Don't bother with a subscription if there are errors already

          build_subscription(plan: @selected_plan, next_renewal_at: @plan_start, creditcard: @creditcard, address: @address)
          unless subscription.valid?
            errors.add(:base, "Error with payment: #{subscription.errors.full_messages.to_sentence}")

            false
          end
        end
      end
    end
  end
end
