# frozen_string_literal: true

module DmSubscriptionsStripe
  module PermittedParams
    #------------------------------------------------------------------------------
    def subscription_plan_params
      params.require(:subscription_plan).permit! if can? :manage_subscriptions, :all
    end

    #------------------------------------------------------------------------------
    def subscription_params
      params.require(:subscription).permit! if can? :manage_subscriptions, :all
    end

    #------------------------------------------------------------------------------
    def make_payment_billing_address_params
      params[:make_payment].require(:billing_address).permit! if can? :manage_subscriptions, :all
    end
  end
end
