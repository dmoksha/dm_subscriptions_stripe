# frozen_string_literal: true

class SubscriptionPlan < ApplicationRecord
  include ActionView::Helpers::NumberHelper

  has_many                  :subscriptions

  monetize                  :price_cents, with_model_currency: :price_currency
  monetize                  :setup_price_cents, with_model_currency: :price_currency

  # --- globalize
  translates                :name, fallbacks_for_empty_translations: true
  globalize_accessors       locales: DmCore::Language.language_array

  # --- FriendlyId
  extend FriendlyId
  include DmCore::Concerns::FriendlyId

  include RankedModel
  ranks                     :row_order, with_same: :account_id

  # renewal_period is the number of months to bill at a time
  # default is 1
  validates_numericality_of :renewal_period, only_integer: true, greater_than: 0
  validates_numericality_of :trial_period, only_integer: true, greater_than_or_equal_to: 0
  validates_inclusion_of    :trial_interval, in: %w[months days]
  validate                  :require_name

  default_scope             { where(account_id: Account.current.id) }

  # for now, always force it to the preferred_default_currency
  #------------------------------------------------------------------------------
  def price_currency
    Account.current.preferred_default_currency
  end

  #------------------------------------------------------------------------------
  def require_name
    errors.add "name_#{Account.current.preferred_default_locale}", I18n.t('errors.messages.blank') if send("name_#{Account.current.preferred_default_locale}").blank?
  end

  #------------------------------------------------------------------------------
  def to_s
    "#{name} - #{amount.format} / month"
  end

  #------------------------------------------------------------------------------
  def amount
    price
  end

  #------------------------------------------------------------------------------
  def setup_amount
    setup_price
  end

  #------------------------------------------------------------------------------
  def trial_period
    self[:trial_period]
  end

  #------------------------------------------------------------------------------
  def revenues
    @revenues ||= subscriptions.group('subscriptions.state').calculate(:sum, :price)
  end
end
