window.StripeBillingSubscriptionManager = class StripeBillingSubscriptionManager
  constructor: ->
    @setupForm()

  #------------------------------------------------------------------------------
  setupForm: ->
    I18n.locale = $('#credit-card-fields').data('locale')

    # Form submit is disabled by default to prevent submission without javascript
    $('#subscription_billing input[type=submit]').attr('disabled', false)
    $('input[name="creditcard[first_name]"]').prop('required',true);
    $('input[name="creditcard[last_name]"]').prop('required',true);

    @initStripeElements()

  #------------------------------------------------------------------------------
  initStripeElements: ->
    stripe   = Stripe($('meta[name="stripe-key"]').attr('content'))
    options  = { locale: I18n.locale }
    elements = stripe.elements(options)

    # Custom styling can be passed to options when creating an Element.
    style = {
      base: {
        fontSize: '16px',
        color: '#32325d',
        fontFamily: '-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, sans-serif',
        fontSmoothing: 'antialiased',
        '::placeholder': {
          color: 'rgba(0,0,0,0.4)'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    }

    # Create an instance of the card Element.
    card = elements.create('card', { style: style });

    # Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

    # Element focus ring
    card.on 'focus', () ->
      el = document.getElementById('card-element')
      el.classList.add('focused')

    card.on 'blur', () ->
      el = document.getElementById('card-element')
      el.classList.remove('focused')

    card.addEventListener 'change', (event) ->
      displayError = document.getElementById('card-errors')
      if event.error
        displayError.textContent = event.error.message
      else
        displayError.textContent = ''


#    zip_code = document.querySelector('input[name="address[zip]"]');
#    zip_code.addEventListener 'change', (event) ->
#      card.update({value: {postalCode: event.target.value}})

    $('#subscription_billing').on 'submit', (evt) =>
      evt.preventDefault()
      @changeLoadingState(true)

      # Initiate payment
      @createPaymentMethodAndCustomer(stripe, card)

#    $('#subscription_billing input[type=submit]').on 'click', (evt) =>
#      evt.preventDefault()
#      @changeLoadingState(true)
#
#      # Initiate payment
#      @createPaymentMethodAndCustomer(stripe, card)


  #------------------------------------------------------------------------------
  showCardError: (error) ->
    @changeLoadingState(false)

    # The card was declined (i.e. insufficient funds, card has expired, etc)
    text = "Unknown Error"

    if error.jserror
      text = I18n['subscriptions'][error.jserror]
    else if error.apierror
      text = error.message || I18n['subscriptions'][error.apierror]
    else if error.message
      text = error.message

    $('#form_error').text(text).show()

    setTimeout ( ->
      $('#form_error').text('')
    ), 8000

  #------------------------------------------------------------------------------
  createPaymentMethodAndCustomer: (stripe, card) =>
    cardholderEmail = document.querySelector('#email').value

    stripe
      .createPaymentMethod('card', card,
        {
          billing_details: {
            email: cardholderEmail
          }
        }
      )
      .then (result) =>
        if result.error
          @showCardError(result.error)
        else
          @createCustomer(stripe, result.paymentMethod.id, cardholderEmail)


  #------------------------------------------------------------------------------
  createCustomer: (stripe, paymentMethod, cardholderEmail) =>
    return fetch('/en/subscriptions/create_customer_and_subscription', {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email: cardholderEmail,
        payment_method: paymentMethod
      })
    })
    .then (response) =>
      return response.json()
    .then (subscription) =>
      @handleSubscription(stripe, subscription)

  #------------------------------------------------------------------------------
  handleSubscription: (stripe, subscription) =>
    latest_invoice = subscription.latest_invoice
    payment_intent = latest_invoice.payment_intent

    if payment_intent
      client_secret = payment_intent.client_secret
      status = payment_intent.status

      if status == 'requires_action' || status == 'requires_payment_method'
        stripe.confirmCardPayment(client_secret)
          .then (result) =>
            if result.error
              # Display error message in your UI.
              # The card was declined (i.e. insufficient funds, card has expired, etc)
              @changeLoadingState(false)
              @showCardError(result.error)
            else
              # Show a success message to your customer
              @confirmSubscription(subscription)

      else
        # No additional information was needed
        # Show a success message to your customer
        @confirmSubscription(subscription)
    else
      @confirmSubscription(subscription)

  #------------------------------------------------------------------------------
  confirmSubscription: (subscription) =>
    return fetch('/en/subscriptions/confirm_subscription', {
      method: 'post',
      headers: { 'Content-type': 'application/json' },
      body: JSON.stringify({ subscription: subscription })
    })
    .then (response) =>
      return response.json()
    .then (subscription) =>
      @orderComplete(subscription)


#* ------- Post-payment helpers -------

  #* Shows a success / error message when the payment is complete
  orderComplete: (subscription) =>
    @changeLoadingState(false)
  #  var subscriptionJson = JSON.stringify(subscription, null, 2);
  #  document.querySelectorAll('.payment-view').forEach(function(view) {
  #    view.classList.add('hidden');
  #  });
  #  document.querySelectorAll('.completed-view').forEach(function(view) {
  #    view.classList.remove('hidden');
  #  });
  #  document.querySelector('.order-status').textContent = subscription.status;
  #  document.querySelector('code').textContent = subscriptionJson;
  #  };

  # Show a spinner on subscription submission
  changeLoadingState: (isLoading) ->
    if isLoading
      # document.querySelector('#spinner').classList.add('loading')
      $('#subscription_billing input[type=submit]').attr('disabled', true)

      # document.querySelector('#button-text').classList.add('hidden')
    else
      $('#subscription_billing input[type=submit]').attr('disabled', false)
      # document.querySelector('#spinner').classList.remove('loading')
      # document.querySelector('#button-text').classList.remove('hidden')





  processCardOld: (stripe, card) ->
    token_data = {}
    token_data['name']            = $('input[name="creditcard[first_name]"]').val() + ' ' + $('input[name="creditcard[last_name]"]').val()
    token_data['address_line1']   = $('input[name="address[address1]"]').val()
    token_data['address_line2']   = $('input[name="address[address2]"]').val()
    token_data['address_city']    = $('input[name="address[city]"]').val()
    token_data['address_state']   = $('input[name="address[state]"]').val()
    token_data['address_zip']     = $('input[name="address[zip]"]').val()
    token_data['address_country'] = $('select[name="address[country]"]').val()

    stripe.createToken(card, token_data).then(@handleStripeResponse.bind(this))

  handleStripeResponseOld: (result) ->
    if result.error
      @errorHandler(result.error)
    else
      # Send the token to your server.
      @stripeTokenHandler(result.token)

  # Submit the form with the token ID.
  stripeTokenHandlerOld: (token) ->
    $('#cardToken').val(token.id)
    $('#card_number').val('')
    $('#card_code').val('')
    $('#credit_card_brand').val(token.card.brand)
    $('#credit_card_country').val(token.card.country)
    $('#credit_card_ip').val(token.client_ip)
    $('#credit_card_ip_country').val('')

    $('#subscription_billing')[0].submit()

  errorHandlerOld: (error) ->
    text = "Unknown Error"
    if error.jserror
      text = I18n['subscriptions'][error.jserror]
    else if error.apierror
      text = error.message || I18n['subscriptions'][error.apierror]

    $('#form_error').text(text).show()
    $('input[type=submit]').attr('disabled', false)
