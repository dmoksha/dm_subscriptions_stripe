# frozen_string_literal: true

module DmSubscriptionsStripe
  class StripeBillingController < DmSubscriptionsStripe::ApplicationController
    protect_from_forgery except: [:create_customer_and_subscription, :confirm_subscription], prepend: true

    skip_before_action :collect_billing_info
    before_action :load_subscription

    # creates a new Customer and attaches the PaymentMethod, then creates a
    # new subscription
    #------------------------------------------------------------------------------
    def create_customer_and_subscription
      render(json: {}, status: :unauthorized) && return unless current_user

      current_customer_id = current_user.current_site_profile.stripe_customer_id

      call_params = { current_user: current_user, payment_method_token: params[:payment_method], subscription_plan: @subscription_plan }

      customer =
        if current_customer_id
          DmSubscriptionsStripe::UpdateCustomerService.call(customer_id: current_customer_id, options: { payment_method_token: params[:payment_method] })
        else
          DmSubscriptionsStripe::CreateCustomerService.call(current_user: current_user, payment_method_token: params[:payment_method])
        end

      current_customer_id ||= customer&.id

      render(json: {}, status: :unauthorized) && return unless current_customer_id

      subscription = DmSubscriptionsStripe::CreateInitialSubscriptionService.call(call_params) if customer.subscriptions.total_count.zero?

      if subscription
        render json: subscription.to_json
      else
        render json: {}, status: :unprocessable_entity
      end
    end

    # confirms that payment was successfully made
    #------------------------------------------------------------------------------
    def confirm_subscription
      subscription = params[:subscription]

      result = DmSubscriptionsStripe::ConfirmSubscriptionService.call(current_user: current_user, subscription: subscription)

      if result
        head :ok
      else
        head :bad_request
      end
    end

    # You can use webhooks to receive information about asynchronous payment events.
    # For more about our webhook events check out https://stripe.com/docs/webhooks.
    #------------------------------------------------------------------------------
    def webhook
      secret = Rails.application.secrets[Account.current.account_prefix]['stripe_webhook_secret']
      event  = DmSubscriptionsStripe::ParseWebhookService.call(request: request, secret: secret)

      DmSubscriptionsStripe::HandleWebhookService.call(event)

      head :ok
    rescue JSON::ParserError
      head :bad_request, status: 400
    rescue Stripe::SignatureVerificationError
      head :bad_request, status: 400
    end

    private

    def load_subscription
      @subscription_plan = current_user.subscription.subscription_plan if current_user
    end
  end
end
