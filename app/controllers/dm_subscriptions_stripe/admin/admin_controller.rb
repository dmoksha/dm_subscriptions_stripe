# frozen_string_literal: true

class DmSubscriptionsStripe::Admin::AdminController < DmCore::Admin::AdminController
  include DmSubscriptionsStripe::PermittedParams
  before_action :authorize_access

  protected

  #------------------------------------------------------------------------------
  def authorize_access
    unless can?(:manage_subscriptions, :all)
      flash[:alert] = 'Unauthorized Access!'
      redirect_to current_account.index_path
    end
  end
end
