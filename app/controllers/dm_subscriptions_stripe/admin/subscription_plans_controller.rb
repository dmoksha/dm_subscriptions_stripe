# frozen_string_literal: true

class DmSubscriptionsStripe::Admin::SubscriptionPlansController < DmSubscriptionsStripe::Admin::AdminController
  include DmSubscriptionsStripe::PermittedParams

  before_action :plan_lookup, except: %i[index new create financials]

  #------------------------------------------------------------------------------
  def index
    @subscription_plans = SubscriptionPlan.rank(:row_order)
    @upccoming_renewals = Subscription.where('amount_cents > 0 AND next_renewal_at >= ? AND next_renewal_at < ?', Time.now - 6.days, Time.now + 2.weeks).order('next_renewal_at ASC')
    @pastdue_renewals   = Subscription.pastdue_renewals(Time.now - 6.days).order('next_renewal_at DESC')
    @updated_plans      = Subscription.where('plan_changed_on > ?', Time.now - 7.days).order('plan_changed_on DESC')
  end

  #------------------------------------------------------------------------------
  def new
    @subscription_plan = SubscriptionPlan.new
  end

  #------------------------------------------------------------------------------
  def edit; end

  #------------------------------------------------------------------------------
  def create
    @subscription_plan = SubscriptionPlan.new(subscription_plan_params)
    if @subscription_plan.save
      redirect_to admin_subscription_plan_path(@subscription_plan), notice: 'Subscription Plan was successfully created.'
    else
      render action: :new
    end
  end

  #------------------------------------------------------------------------------
  def update
    if @subscription_plan.update(subscription_plan_params)
      redirect_to admin_subscription_plan_path(@subscription_plan), notice: 'Subscription Plan was successfully updated.'
    else
      render action: :edit
    end
  end

  #------------------------------------------------------------------------------
  def financials; end

  #------------------------------------------------------------------------------
  def show
    @fully_paid = @subscription_plan.subscriptions.fully_paid
    @lapsed     = @subscription_plan.subscriptions.lapsed
    @never_paid = @subscription_plan.subscriptions.never_paid
  end

  #------------------------------------------------------------------------------
  def sort
    @subscription_plan.update_attribute(:row_order_position, params[:item][:row_order_position])

    #--- this action will be called via ajax
    head :ok
  end

  private

  #------------------------------------------------------------------------------
  def plan_lookup
    @subscription_plan = SubscriptionPlan.friendly.find(params[:id])
  end
end
