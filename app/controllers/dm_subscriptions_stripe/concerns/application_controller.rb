# frozen_string_literal: true

module DmSubscriptionsStripe
  module Concerns
    module ApplicationController
      extend ActiveSupport::Concern

      included do
        before_action :collect_billing_info
      end

      # Redirect to the billing page if the user needs to enter
      # payment info before being able to use the app
      #------------------------------------------------------------------------------
      def collect_billing_info
        return unless user_signed_in?
        return if self.class.to_s.match(/^Devise/)      # Don't prevent logins, etc.
        return if current_user.is_admin?                # Admins don't need to pay
        # return unless account = current_account       # Nothing to do if there is no account

        return if !(sub = current_user.subscription) || sub.state.nil?
        return if sub.state == 'active' && sub.current? && !sub.needs_payment_info?
        return if sub.state == 'active' && sub.amount == 0
        return if sub.state == 'trial' && sub.current? && (!DmSubscriptionsStripe.config.require_payment_info_for_trials || !sub.needs_payment_info?)

        redirect_to main_app.subscription_billing_path
      end

      module ClassMethods
      end
    end
  end
end
