# frozen_string_literal: true

# Typically used in a Devise::RegistrationController.
#------------------------------------------------------------------------------
module DmSubscriptionsStripe
  module Concerns
    module PlanSignupController
      extend ActiveSupport::Concern

      included do
        before_action       :load_plan,           only: %i[new create]
        after_action        :build_plan,          only: [:create]
        skip_before_action  :collect_billing_info
      end

      protected

      # it's ok if the plan is not found - means they are not subscribed
      #------------------------------------------------------------------------------
      def load_plan
        if params[:plan].present?
          @plan = SubscriptionPlan.friendly.find(params[:plan])
        elsif params[:user].present? && params[:user][:plan].present?
          @plan = SubscriptionPlan.friendly.find(params[:user][:plan])
        end
      end

      # it's ok if the plan is not found - means they are not subscribed
      #------------------------------------------------------------------------------
      def build_plan
        @plan = SubscriptionPlan.friendly.find(params[:user][:plan]) if resource.present?
      end

      module ClassMethods
      end
    end
  end
end
