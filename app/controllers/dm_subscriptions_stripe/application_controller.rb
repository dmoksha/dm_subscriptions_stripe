# frozen_string_literal: true

# Subclass from main ApplicationController, which will subclass from DmCore
#------------------------------------------------------------------------------
class DmSubscriptionsStripe::ApplicationController < ::ApplicationController
  layout 'subscriptions'
end
