class AddStripePlanId < ActiveRecord::Migration[5.0]
  def change
    add_column :subscription_plans, :stripe_plan_id, :string
  end
end
