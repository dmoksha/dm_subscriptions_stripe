class AddStripeCustomerId < ActiveRecord::Migration[5.0]
  def change
    add_column :user_site_profiles, :stripe_customer_id, :string, null: true
    add_index  :user_site_profiles, :stripe_customer_id, unique: true
  end
end
