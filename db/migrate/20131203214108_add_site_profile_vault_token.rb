# frozen_string_literal: true

class AddSiteProfileVaultToken < ActiveRecord::Migration[4.2]
  def change
    add_column      :user_site_profiles,      :customer_token,  :string
    rename_column   :sub_user_subscriptions,  :vault_token,     :subscription_token
  end
end
