# frozen_string_literal: true

# By placing all of MokshaCms's shared dependencies in this file and then loading
# it for each component's Gemfile, we can be sure that we're only testing just
# the one component of MokshaCms.
source 'https://rubygems.org'

# rubocop:disable Bundler/OrderedGems
gem 'rails', '~> 6.1.6'

gem 'dm_preferences',         '~> 1.6.0'
gem 'themes_for_rails',       git: 'https://github.com/digitalmoksha/themes_for_rails.git'
gem 'aced_rails',             git: 'https://github.com/digitalmoksha/aced_rails.git'

gem 'dm_core',                git: 'https://gitlab.com/moksha_cms/moksha_cms.git', branch: '6-1-stable'
# rubocop:enable Bundler/OrderedGems

# personal development gems
# local_path = '~/dev/_gitlab/dmoksha'
# gem 'dm_core',                path: "#{local_path}/moksha_cms/core"

gem 'bootsnap', '>= 1.13.0', require: false
gem 'coffee-rails', '~> 5.0'
gem 'mini_racer', '~> 0.6.3', platforms: :ruby
gem 'puma', '~> 5.6.5'
gem 'sass-rails', '>= 6'
gem 'uglifier', '>= 4.2.0'
gem 'webpacker', '~> 5.4'

gemspec

group :development, :test do
  gem 'pry-byebug'
  gem 'sqlite3', '~> 1.5'

  gem 'factory_bot_rails', '~> 6.2'
  gem 'mocha', '~> 1.15', require: false
  gem 'rspec-rails', '~> 5.1'

  gem 'rubocop', '~> 1.36'
  gem 'rubocop-rspec', '~> 2.13'
end

group :development do
  gem 'listen'
  gem 'spring'
  gem 'web-console', '>= 4.2'
end

group :test do
  gem 'capybara', '>= 3.37'
  gem 'database_cleaner', '~> 2.0'
  gem 'faker', '~> 2.23'
  gem 'launchy', '~> 2.5'
  gem 'rails-controller-testing'
  gem 'rspec-formatter-webkit'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers'
  gem 'stripe-ruby-mock', '~> 3.0.1', require: 'stripe_mock'
  gem 'syntax'
  gem 'test-prof'
  gem 'webdrivers'
  gem 'webmock', '~> 3.18'
end
