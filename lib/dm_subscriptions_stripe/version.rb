# frozen_string_literal: true

module DmSubscriptionsStripe
  VERSION = '6.1.0'
end
