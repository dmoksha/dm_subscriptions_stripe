# frozen_string_literal: true

require 'stripe'
require 'active_merchant'
require 'dm_subscriptions/extensions/active_merchant'

module DmSubscriptionsStripe
  class Engine < ::Rails::Engine
    isolate_namespace DmSubscriptionsStripe

    config.to_prepare do
      # require any concerns that need to be injected
      require_dependency 'dm_subscriptions_stripe/model_decorators'
      require_dependency 'dm_subscriptions_stripe/concerns/acts_as_subscription'
      require_dependency 'dm_subscriptions_stripe/concerns/application_controller'
    end

    config.generators do |g|
      g.test_framework      :rspec, fixture: false
      g.fixture_replacement :factory_bot, dir: 'spec/factories'
      g.assets false
      g.helper false
    end
  end
end
