# frozen_string_literal: true

require 'dm_subscriptions_stripe/engine'

module DmSubscriptionsStripe
  #------------------------------------------------------------------------------
  class << self
    def config
      @config ||= Config.new
      yield(@config) if block_given?
      @config
    end
  end

  #------------------------------------------------------------------------------
  class Config
    attr_accessor :require_payment_info_for_trials

    #------------------------------------------------------------------------------
    def initialize
      @require_payment_info_for_trials = false
    end
  end
end
