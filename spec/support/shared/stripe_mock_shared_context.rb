# frozen_string_literal: true

RSpec.shared_context 'setup stripe mocking' do
  let_it_be(:api)     { DmSubscriptionsStripe::PaymentApiStripeSubscription.new }
  let(:stripe_helper) { StripeMock.create_test_helper }

  before(:all) do
    Account.current = FactoryBot.create(:stripe_subscription_account)

    config_stripe_keys
    live_stripe_message

    StripeMock.toggle_live(live_stripe_test?)
    StripeMock.start
  end

  after(:all) do
    StripeMock.stop
  end

  def config_stripe_keys
    Rails.application.secrets[Account.current.account_prefix] =
      if ENV['STRIPE_PUBLIC_KEY'].present? && ENV['STRIPE_PRIVATE_KEY'].present?
        { 'stripe_public_key' => ENV['STRIPE_PUBLIC_KEY'], 'stripe_private_key' => ENV['STRIPE_PRIVATE_KEY'] }
      else
        { 'stripe_public_key' => 'test-public-key', 'stripe_private_key' => 'test-private-key' }
      end
  end

  def live_stripe_test?
    ENV['STRIPE_PUBLIC_KEY'].present? && ENV['STRIPE_PRIVATE_KEY'].present?
  end

  def live_stripe_message
    puts live_stripe_test? ? '  *** Using Stripe Test Server' : '  *** Mocking Stripe Server'
  end
end
