# frozen_string_literal: true

def valid_address(attributes = {})
  {
    first_name: 'John',
    last_name: 'Doe',
    address1: '2010 Cherry Ct.',
    city: 'Mobile',
    state: 'AL',
    zip: '36608',
    country: 'US'
  }.merge(attributes)
end

def valid_card(attributes = {})
  { first_name: 'Joe',
    last_name: 'Doe',
    month: 2,
    year: Time.now.year + 1,
    number: '1',
    brand: 'bogus',
    verification_value: '123' }.merge(attributes)
end

# def valid_user(attributes = {})
#   FactoryBot.build(:user, attributes).attributes
# end

def valid_subscription(attributes = {})
  { plan: FactoryBot.build(:subscription_plan),
    subscriber: FactoryBot.build(:user) }.merge(attributes)
end
