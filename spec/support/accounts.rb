# frozen_string_literal: true

module AccountMacros
  # Setup the current account.  Used for model specs.  Need to use :each, otherwise
  # the created Account does not get cleared between runs
  #------------------------------------------------------------------------------
  def setup_account
    before_all do
      Account.current = FactoryBot.create(:account)
    end
  end
end

#------------------------------------------------------------------------------
RSpec.configure do |config|
  config.extend  AccountMacros, type: :model
  config.extend  AccountMacros, type: :helper
  config.extend  AccountMacros, type: :service
  config.extend  AccountMacros, type: :mailer
  config.extend  AccountMacros, type: :uploader
  config.extend  AccountMacros, type: :liquid_tag
  config.extend  AccountMacros, type: :rake_task
end
