# frozen_string_literal: true

require 'spec_helper'
include ActionView::Helpers::NumberHelper

describe SubscriptionPlan do
  setup_account

  fixtures :subscription_plans

  before(:each) do
    @plan = create(:subscription_plan)
  end

  it 'should return a formatted name' do
    expect(@plan.to_s).to eq("#{@plan.name} - #{@plan.amount.format} / month")
  end

  it 'should return the name for URL params' do
    # @plan.to_param.should == @plan.name
    expect(@plan.slug).to eq('basic')
  end

  it 'should return the full amount' do
    expect(@plan.amount).to eq(@plan.price)
  end

  it 'should return the default trial period' do
    expect(@plan.trial_period).to eq(1)
  end
end
