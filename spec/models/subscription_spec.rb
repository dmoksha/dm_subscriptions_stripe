# frozen_string_literal: true

require 'spec_helper'
include ActiveMerchant::Billing

describe Subscription do
  setup_account

  fixtures :subscription_plans
  fixtures :subscriptions

  before(:all) do
    @basic = FactoryBot.create(:subscription_plan)
  end
  before(:each) do
    @basic = FactoryBot.create(:subscription_plan)
  end

  it 'should be created as a trial by default' do
    s = Subscription.new(plan: @basic)
    expect(s.state).to eq('trial')
  end

  it 'should be created as active with plans that are free and no trial period' do
    @basic.price_cents = 0
    @basic.trial_period = 0
    s = Subscription.new(plan: @basic)
    expect(s.state).to eq('active')
  end

  it 'should be created with a renewal date a month from now by default' do
    s = Subscription.create(plan: @basic)
    expect(s.next_renewal_at.localtime.at_midnight).to eq(Time.now.advance(months: 1).at_midnight)
  end

  it 'should be created with a specified renewal date' do
    s = Subscription.create(plan: @basic, next_renewal_at: 1.day.from_now)
    expect(s.next_renewal_at.localtime.at_midnight).to eq(Time.now.advance(days: 1).at_midnight)
  end

  it 'should return the amount in pennies' do
    s = Subscription.new(amount: 10)
    expect(s.amount.cents).to eq(1000)
  end

  it 'should set values from the assigned plan' do
    s = Subscription.new(plan: @basic)
    expect(s.amount).to eq(@basic.amount)
    # s.user_limit.should == @basic.user_limit   --- don't have limits in DB right now
  end

  it 'should need payment info when no card is saved and the plan is not free' do
    expect(Subscription.new(plan: @basic).needs_payment_info?).to be true
  end

  it 'should not need payment info when the card is saved and the plan is not free' do
    expect(Subscription.new(plan: @basic, card_number: 'foo').needs_payment_info?).to be false
  end

  it 'should not need payment info when no card is saved but the plan is free' do
    @basic.price = 0
    expect(Subscription.new(plan: @basic).needs_payment_info?).to be false
  end

  # [todo] this needs to be re-thought - what are we actually wanting to test here?
  # The "expects" is failing, but why test the internal implementation - should be testing
  # the results I think...
  # it "should find expiring trial subscriptions" do
  #   Subscription.expects(:find).with(:all, :include => :subscriber,
  #     :conditions => { :state => 'trial', :next_renewal_at => (7.days.from_now.beginning_of_day .. 7.days.from_now.end_of_day) })
  #   Subscription.find_expiring_trials
  # end
  #
  # it "should find active subscriptions needing payment" do
  #   Subscription.expects(:find).with(:all, :include => :subscriber,
  #     :conditions => { :state => 'active', :next_renewal_at => (Time.now.beginning_of_day .. Time.now.end_of_day) })
  #   Subscription.find_due
  # end
  #
  # it "should find active subscriptions needing payment in the past" do
  #   Subscription.expects(:find).with(:all, :include => :subscriber,
  #     :conditions => { :state => 'active', :next_renewal_at => (2.days.ago.beginning_of_day .. 2.days.ago.end_of_day) })
  #   Subscription.find_due(2.days.ago)
  # end

  describe 'when being created' do
    before(:each) do
      @sub = Subscription.new(plan: @basic)
    end

    describe 'without a credit card' do
      it 'should not include card storage in the validation' do
        # @sub.expects(:store_card).never   --- [todo] don't understand what this test is supposed to do
        expect(@sub).to be_valid
      end
    end

    describe 'with a credit card' do
      before(:each) do
        @sub.creditcard = @card = CreditCard.new(valid_card)
        @sub.address = @address = SubscriptionAddress.new(valid_address)
        @payment_api = DmSubscriptionsStripe::PaymentApiBogus.new
        allow(@payment_api).to receive(:gateway).and_return(@gw = ActiveMerchant::Billing::Base.gateway(:bogus).new)
        allow(@sub).to receive(:payment_api).and_return(@payment_api)
      end

      it 'should include card storage in the validation' do
        expect(@sub).to receive(:store_card).with(@card, billing_address: @address.to_activemerchant).and_return(true)
        expect(@sub).to be_valid
      end

      it 'should not be valid if the card storage fails' do
        expect(@gw).to receive(:store).and_return(Response.new(false, 'Forced failure'))
        expect(@sub).not_to be_valid
        expect(@sub.errors.full_messages).to include('Forced failure')
      end

      it 'should not be valid if billing fails' do
        @sub.subscription_plan.trial_period = nil
        expect(@gw).to receive(:store).and_return(Response.new(true, 'Forced success'))
        expect(@gw).to receive(:purchase).and_return(Response.new(false, 'Purchase failure'))
        expect(@sub).not_to be_valid
        expect(@sub.errors.full_messages).to include('Purchase failure')
      end

      describe 'storing the card' do
        before(:each) do
          @time = Time.now.utc
          allow(Time).to receive(:now).and_return(@time)
        end

        after(:each) do
          expect(@sub).to be_valid
        end

        it 'should keep the subscription in trial state' do
          expect(@sub).not_to receive(:state=)
        end

        it 'should not save the subscription' do
          expect(@sub).not_to receive(:save)
        end

        it 'should set the renewal date if not set' do
          expect(@sub).to receive(:next_renewal_at=).with(@time.advance(months: 1))
        end

        it 'should set the renewal date based on the trial period' do
          @sub.subscription_plan.trial_period = 2
          expect(@sub).to receive(:next_renewal_at=).with(@time.advance(months: 2))
        end

        it 'should set the renewal date based on the trial interval' do
          @sub.subscription_plan.trial_interval = 'days'
          @sub.subscription_plan.trial_period = 15
          expect(@sub).to receive(:next_renewal_at=).with(@time.advance(days: 15))
        end

        it 'should keep the renewal date when previously set' do
          @sub.next_renewal_at = 1.day.from_now
          expect(@sub).not_to receive(:next_renewal_at=)
        end

        it 'should not bill now if there is a trial period' do
          @sub.subscription_plan.trial_period = 1
          expect(@gw).not_to receive(:purchase)
        end

        describe 'without a trial period' do
          before(:each) do
            @sub.subscription_plan.trial_period = nil
            @response = Response.new(true, 'Forced Success')
            allow_any_instance_of(SubscriptionPayment).to receive(:send_receipt).and_return(true)
          end

          it 'should bill now' do
            expect(@gw).to receive(:purchase).and_return(@response)
          end

          it 'should bill the setup amount, if any' do
            @sub.subscription_plan.setup_price = 500
            expect(@gw).to receive(:purchase).with(@sub.subscription_plan.setup_amount.cents, any_args).and_return(@response)
          end

          it 'should bill the plan amount, if no setup amount' do
            @sub.subscription_plan.setup_price = nil
            expect(@gw).to receive(:purchase).with(@sub.subscription_plan.amount.cents, any_args).and_return(@response)
          end

          it 'should record the charge with the setup amount' do
            @sub.subscription_plan.setup_price = 500
            expect(@gw).to receive(:purchase).and_return(@response)
            expect((ary = [])).to receive(:build).with(hash_including(amount: @sub.subscription_plan.setup_amount, setup: true))
            expect(@sub).to receive(:subscription_payments).and_return(ary)
          end

          it 'should record the charge with the plan amount' do
            @sub.subscription_plan.setup_price = nil
            expect(@gw).to receive(:purchase).and_return(@response)
            expect((ary = [])).to receive(:build).with(hash_including(amount: @sub.subscription_plan.amount, setup: false))
            expect(@sub).to receive(:subscription_payments).and_return(ary)
          end

          it "should set the renewal date based on the plan's renewal period if a free plan and no trial" do
            @basic.trial_period = nil
            @basic.renewal_period = 3
            @basic.price = 0
            @sub = Subscription.new(plan: @basic)
            expect(@sub).to receive(:next_renewal_at=).with(@time.advance(months: 3))
            expect(@sub.save).to be_truthy
          end

          it 'should set the renewal date to now if no trial, to ensure payment' do
            @basic.trial_period = nil
            @basic.renewal_period = 3
            @sub = Subscription.new(plan: @basic)
            expect(@sub).to receive(:next_renewal_at=).with(@time)
            expect(@sub.save).to be_truthy
          end
        end
      end
    end
  end

  describe '' do
    before(:each) do
      # @sub = subscriptions(:one)
      @sub = FactoryBot.create(:subscription)
    end

    it 'should reflect the assigned amount, not the amount from the plan' do
      @sub.update_attribute(:amount, @sub.subscription_plan.amount - 1.to_money('EUR'))
      @sub.reload
      expect(@sub.amount).to eq(@sub.subscription_plan.amount - 1.to_money('EUR'))
    end

    describe 'when destroyed' do
      before(:each) do
        @payment_api = DmSubscriptionsStripe::PaymentApiBogus.new
        allow(@payment_api).to receive(:gateway).and_return(@gw = ActiveMerchant::Billing::Base.gateway(:bogus).new)
        allow(@sub).to receive(:payment_api).and_return(@payment_api)
      end

      it 'should delete the stored card info at the gateway' do
        expect(@gw).to receive(:unstore).with(@sub.billing_id).and_return(true)
        @sub.destroy
      end

      it 'should not attempt to delete the stored card info with no billing id' do
        @sub.billing_id = nil
        expect(@gw).not_to receive(:unstore)
        @sub.destroy
      end
    end

    describe 'when failing to store the card' do
      it 'should return false and set the error message to the processor response' do
        @payment_api = DmSubscriptionsStripe::PaymentApiBogus.new
        allow(@payment_api).to receive(:gateway).and_return(@gw = ActiveMerchant::Billing::Base.gateway(:bogus).new)
        allow(@sub).to receive(:payment_api).and_return(@payment_api)
        @response = Response.new(false, 'Forced failure')
        expect(@gw).to receive(:update).and_return(@response)
        @card = double('CreditCard', display_number: '1111', expiry_date: CreditCard::ExpiryDate.new(5, 2012))
        expect(@sub.store_card(@card)).to be_falsey
        expect(@sub.errors.full_messages).to include('Forced failure')
      end
    end

    describe 'when storing the credit card' do
      describe 'successfully' do
        before(:each) do
          @time = Time.now
          @payment_api = DmSubscriptionsStripe::PaymentApiBogus.new
          allow(@payment_api).to receive(:gateway).and_return(@gw = ActiveMerchant::Billing::Base.gateway(:bogus).new)
          allow(@sub).to receive(:payment_api).and_return(@payment_api)
          @response = ActiveMerchant::Billing::Response.new(true, 'Forced success', { 'customer_vault_id' => '123' }, authorization: 'foo')
          @card = double('CreditCard', display_number: '1111', expiry_date: CreditCard::ExpiryDate.new(5, 2012))
          allow(Time).to receive(:now).and_return(@time)
        end

        after(:each) do
          expect(@sub).to receive(:card_number=).with('1111')
          expect(@sub).to receive(:card_expiration=).with('05-2012')
          expect(@sub).to receive(:state=).with('active')
          expect(@sub).to receive(:save)
          expect(@sub.store_card(@card)).to be_truthy
        end

        describe 'for the first time' do
          before(:each) do
            @sub.card_number = nil
            @sub.billing_id = nil
          end

          it 'should store the card and store the billing id' do
            expect(@gw).to receive(:store).with(@card, {}).and_return(@response)
            expect(@sub).to receive(:billing_id=).with('foo')
            allow(@gw).to receive(:purchase).and_return(@response)
          end

          it 'should bill the amount and set the renewal date a month hence with a renewal date in the past' do
            @sub.next_renewal_at = 2.days.ago
            allow(@gw).to receive(:store).and_return(@response)
            expect(@gw).to receive(:purchase).with(@sub.amount.cents, @response.authorization, anything).and_return(@response)
            expect(@sub).to receive(:next_renewal_at=).with(@time.advance(months: 1))
          end

          it 'should bill the amount and set the renewal date a month hence and with no renewal date' do
            @sub.next_renewal_at = nil
            allow(@gw).to receive(:store).and_return(@response)
            expect(@gw).to receive(:purchase).with(@sub.amount.cents, @response.authorization, anything).and_return(@response)
            expect(@sub).to receive(:next_renewal_at=).with(@time.advance(months: 1))
          end

          it 'should not bill and not change the renewal date with a renewal date in the future' do
            @sub.next_renewal_at = @time.advance(days: 2)
            allow(@gw).to receive(:store).and_return(@response)
            expect(@gw).to receive(:purchase).never
            expect(@sub).not_to receive(:next_renewal_at=)
          end

          it 'should record the charge with no renewal date' do
            @sub.next_renewal_at = nil
            allow(@gw).to receive(:store).and_return(@response)
            expect(@gw).to receive(:purchase).with(@sub.amount.cents, @response.authorization, anything).and_return(@response)
            expect((ary = [])).to receive(:build).with(hash_including(amount: @sub.amount))
            expect(@sub).to receive(:subscription_payments).and_return(ary)
          end

          it 'should not record a charge with a renewal date in the future' do
            @sub.next_renewal_at = @time.advance(days: 2)
            allow(@gw).to receive(:store).and_return(@response)
            expect(@gw).not_to receive(:purchase)
            expect(@sub).not_to receive(:subscription_payments)
          end
        end

        describe 'subsequent times' do
          before(:each) do
            allow(@gw).to receive(:update).and_return(@response)
            allow(@gw).to receive(:purchase).and_return(@response)
          end

          it 'should not overwrite the billing_id' do
            expect(@gw).not_to receive(:billing_id=)
          end

          it 'should update the vault when updating an existing card' do
            expect(@gw).to receive(:update).with(@sub.billing_id, @card, {}).and_return(@response)
          end

          it 'should make a purchase and set the renewal date a month hence with a renewal date in the past' do
            @sub.next_renewal_at = 2.days.ago
            expect(@gw).to receive(:purchase).with(@sub.amount.cents, @sub.billing_id, anything).and_return(@response)
            expect(@sub).to receive(:next_renewal_at=).with(@time.advance(months: 1))
          end

          it 'should make a purchase and set the renewal date a month hence and with no renewal date' do
            @sub.next_renewal_at = nil
            expect(@gw).to receive(:purchase).with(@sub.amount.cents, @sub.billing_id, anything).and_return(@response)
            expect(@sub).to receive(:next_renewal_at=).with(@time.advance(months: 1))
          end

          it 'should not call the gateway and not change the renewal date with a renewal date in the future' do
            @sub.next_renewal_at = @time.advance(days: 2)
            expect(@gw).not_to receive(:purchase)
            expect(@sub).not_to receive(:next_renewal_at=)
          end

          it 'should record the charge with no renewal date' do
            @sub.next_renewal_at = nil
            expect(@gw).to receive(:purchase).with(@sub.amount.cents, @sub.billing_id, anything).and_return(@response)
            expect((ary = [])).to receive(:build).with(hash_including(amount: @sub.amount))
            expect(@sub).to receive(:subscription_payments).and_return(ary)
          end

          it 'should not record a charge with a renewal date in the future' do
            @sub.next_renewal_at = @time.advance(days: 2)
            expect(@gw).not_to receive(:purchase)
            expect(@sub).not_to receive(:subscription_payments)
          end
        end

        describe 'sends receipt' do
          before(:each) do
            @time = Time.now
            @payment_api = DmSubscriptionsStripe::PaymentApiBogus.new
            allow(@payment_api).to receive(:gateway).and_return(@gw = ActiveMerchant::Billing::Base.gateway(:bogus).new)
            allow(@sub).to receive(:payment_api).and_return(@payment_api)
            @response = Response.new(true, 'Forced success', { 'customer_vault_id' => '123' }, 'authorization' => 'foo')
            @card = double('CreditCard', display_number: '1111', expiry_date: CreditCard::ExpiryDate.new(5, 2012))
            allow(Time).to receive(:now).and_return(@time)

            allow(@gw).to receive(:update).and_return(@response)
            allow(@gw).to receive(:purchase).and_return(@response)

            @emails = ActionMailer::Base.deliveries
            @emails.clear
          end

          it 'unless there was no charge' do
            @sub.next_renewal_at = @time.advance(days: 2)
            expect { expect(@sub.store_card(@card)).to be_truthy }.to change(SubscriptionPayment, :count).by(0)
            expect(ActionMailer::Base.deliveries.count).to eq(0)
          end

          # [todo]
          # it "with an end date in the future if the renewal date was in the past" do
          #   @sub.next_renewal_at = 2.days.ago
          #   Account.any_instance.stubs(:email).and_return("test@email.com")
          #   lambda { @sub.store_card(@card).should be_truthy }.should change(SubscriptionPayment, :count).by(1)
          #   ActionMailer::Base.deliveries.count.should == 1
          #   email = ActionMailer::Base.deliveries.last
          #   email.body.should include("From #{Time.now.to_s(:short_day).strip} to #{Time.now.advance(:months => 1).to_s(:short_day).strip}")
          # end
        end
      end
    end

    describe 'when switching plans' do
      before(:each) do
        @plan = subscription_plans(:advanced)
      end

      # it "should allow switching to a plan with no user limit" do
      #   @plan.expects(:user_limit).and_return(nil).at_least_once
      #   @sub.plan = @plan
      #   @sub.valid?.should be true
      # end

      # it "should refuse switching to a plan with a user limit less than the current number of users" do
      #   @plan.expects(:user_limit).and_return(2).at_least_once
      #   @sub.subscriber.stubs(:users).and_return(mock('users').tap {|m| m.expects(:count).and_return(3).at_least_once })
      #   @sub.plan = @plan
      #   @sub.valid?.should be false
      #   @sub.errors.full_messages.should include('User limit for new plan would be exceeded.')
      # end

      # it "should allow switching to a plan with a user limit greater than the current number of users" do
      #   @plan.expects(:user_limit).and_return(2).at_least_once
      #   @sub.subscriber.stubs(:users).and_return(mock('users').tap {|m| m.expects(:count).and_return(2).at_least_once })
      #   @sub.plan = @plan
      #   @sub.valid?.should be true
      # end
    end

    describe 'when charging' do
      before(:each) do
        @payment_api = DmSubscriptionsStripe::PaymentApiBogus.new
        allow(@payment_api).to receive(:gateway).and_return(@gw = ActiveMerchant::Billing::Base.gateway(:bogus).new)
        allow(@sub).to receive(:payment_api).and_return(@payment_api)
        allow_any_instance_of(@sub.subscriber.class).to receive(:email).and_return('some@email.com')
        @response = Response.new(true, 'Forced success', { authorized_amount: @sub.amount.cents.to_s }, test: true, authorization: '411')
      end

      it 'should charge nothing for free accounts and update the renewal date and state' do
        @sub.amount = 0
        expect(@gw).not_to receive(:purchase)
        expect(@sub).to receive(:update).with(next_renewal_at: @sub.next_renewal_at.advance(months: 1), state: 'active')
        expect(@sub.charge).to be_truthy
      end

      it 'should not record a payment when charging nothing' do
        @sub.amount = 0
        expect(@sub).not_to receive(:subscription_payments)
        expect(@sub.charge).to be_truthy
      end

      # it "should charge for paid accounts and update the renewal date and state" do
      #   @gw.expects(:purchase).with(@sub.amount.cents, @sub.billing_id, anything).and_return(@response)
      #   @sub.expects(:update).with(:next_renewal_at => @sub.next_renewal_at.advance(:months => 1), :state => 'active')
      #   @sub.charge.should be_truthy
      # end

      # it "should record the payment when charging paid accounts" do
      #   @gw.expects(:purchase).with(@sub.amount.cents, @sub.billing_id, anything).and_return(@response)
      #   lambda { @sub.charge.should be_truthy }.should change(SubscriptionPayment, :count).by(1)
      #   sp = SubscriptionPayment.last
      #   { :amount => @sub.amount, :subscriber => @sub.subscriber, :transaction_id => @response.authorization}.each do |meth, val|
      #     sp.send(meth).should == val
      #   end
      # end

      it 'should return an error when the processor fails' do
        expect(@gw).to receive(:purchase).with(@sub.amount.cents, @sub.billing_id, anything).and_return(Response.new(false, 'Oops'))
        expect(@sub).not_to receive(:update_attribute)
        expect(@sub).not_to receive(:subscription_payments)
        expect(@sub.charge).to be_falsey
        expect(@sub.errors.full_messages).to include('Oops')
      end
    end
  end
end
