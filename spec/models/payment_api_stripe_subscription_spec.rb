# frozen_string_literal: true

require 'spec_helper'

describe DmSubscriptionsStripe::PaymentApiStripeSubscription do
  include_context 'setup stripe mocking'

  let_it_be(:user) { create(:user) }

  it 'has correct payment processor set' do
    expect(api.processor).to eq Account.current.preferred_subscription_processor
  end

  it 'has valid stripe api' do
    expect(api.class).to eq DmSubscriptionsStripe::PaymentApiStripeSubscription
  end

  context 'using Stripe account' do
    before(:all) do
      @product = create_test_product
      @plan    = create_test_plan
    end

    after(:all) do
      delete_test_plan
      delete_test_product
    end

    it 'has valid stripe keys' do
      expect(api.public_key).to_not eq nil
      expect(api.api_key).to_not eq nil
    end

    describe '#create_customer' do
      it 'creates a new customer with payment method' do
        payment_method = create_payment_method
        customer = api.create_customer(user: user, payment_method_token: payment_method.id,
                                       options: { description: 'New client' })

        expect(customer).to_not be_nil
        expect(customer.id).to match(/^cus_/) if live_stripe_test?
        expect(customer.email).to eq user.email
        expect(customer.name).to eq user.full_name
        expect(customer.description).to eq('New client')
        expect(customer.invoice_settings.default_payment_method).to eq payment_method.id
        expect(customer.metadata.user_id.to_i).to eq user.id

        delete_customer(customer)
      end

      it 'does not creates a new customer with bogus payment method' do
        customer = api.create_customer(user: user, payment_method_token: 'bogus payment',
                                       options: { description: 'New client' })

        expect(customer).to be_nil if live_stripe_test?
      end
    end

    describe '#update_customer' do
      it 'creates a new customer with payment method' do
        payment_method = create_payment_method
        customer = api.create_customer(user: user, payment_method_token: payment_method.id,
                                       options: { description: 'New client' })

        expect(customer).to_not be_nil
        expect(customer.description).to eq('New client')
        expect(customer.invoice_settings.default_payment_method).to eq payment_method.id
        expect(customer.metadata.user_id.to_i).to eq user.id

        customer = api.update_customer(customer_id: customer.id,
                                       options: { description: 'Updated description' })

        expect(customer).to_not be_nil
        expect(customer.description).to eq('Updated description')

        delete_customer(customer)
      end
    end

    describe '#attach_payment_method' do
      it 'attaches a payment method and sets as default' do
        payment_method = create_payment_method
        customer = api.create_customer(user: user, payment_method_token: payment_method.id,
                                       options: { description: 'New client' })

        payment_method = create_payment_method

        customer = api.attach_payment_method(customer_id: customer.id, payment_method_token: payment_method.id)

        expect(customer).to_not be_nil
        expect(customer.id).to match(/^cus_/) if live_stripe_test?
        expect(customer.invoice_settings.default_payment_method).to eq payment_method.id

        delete_customer(customer)
      end
    end

    describe '#create_subscription_to_plan' do
      it 'creates a new subscription for a customer' do
        if live_stripe_test?
          payment_method = create_payment_method
          customer = api.create_customer(user: user, payment_method_token: payment_method.id,
                                         options: { description: 'New client' })

          subscription = api.create_subscription_to_plan(customer.id, @plan.id)

          expect(subscription).to_not be_nil
          expect(subscription.id).to match(/^sub_/)
          expect(subscription.object).to eq 'subscription'
          expect(subscription.collection_method).to eq 'charge_automatically'
          expect(subscription.customer).to eq customer.id
          expect(subscription.plan.id).to eq @plan.id
          expect(subscription.status).to eq 'active'

          cancel_subscription(subscription)
          delete_customer(customer)
        end
      end

      it 'does not creates a new subscription with bogus customer' do
        subscription = api.create_subscription_to_plan('not_a_real_customer', @plan.id)

        expect(subscription).to be_nil
      end

      it 'creates a new subscription but requires authentication' do
        if live_stripe_test?
          payment_method = create_payment_method(requires_authentication_card)
          customer = api.create_customer(user: user, payment_method_token: payment_method.id,
                                         options: { description: 'New client' })

          subscription = api.create_subscription_to_plan(customer.id, @plan.id)

          expect(subscription).to_not be_nil
          expect(subscription.id).to match(/^sub_/)
          expect(subscription.object).to eq 'subscription'
          expect(subscription.collection_method).to eq 'charge_automatically'
          expect(subscription.customer).to eq customer.id
          expect(subscription.plan.id).to eq @plan.id
          expect(subscription.status).to eq 'incomplete'

          cancel_subscription(subscription)
          delete_customer(customer)
        end
      end

      it 'does not create a new subscription but insufficient funds' do
        if live_stripe_test?
          payment_method = create_payment_method(insufficient_funds_card)
          customer = api.create_customer(user: user, payment_method_token: payment_method.id,
                                         options: { description: 'New client' })

          subscription = api.create_subscription_to_plan(customer.id, @plan.id)

          expect(subscription).to_not be_nil
          expect(subscription.id).to match(/^sub_/)
          expect(subscription.object).to eq 'subscription'
          expect(subscription.collection_method).to eq 'charge_automatically'
          expect(subscription.customer).to eq customer.id
          expect(subscription.plan.id).to eq @plan.id
          expect(subscription.status).to eq 'incomplete'

          cancel_subscription(subscription)
          delete_customer(customer)
        end
      end
    end
  end

  def create_payment_method(card = successful_card)
    Stripe::PaymentMethod.create({ type: 'card', card: card }, api.api_key)
  end

  def successful_card(year: '2024')
    {
      number: '4242424242424242',
      exp_month: 11,
      exp_year: year,
      cvc: '314'
    }
  end

  def requires_authentication_card(year: '2024')
    {
      number: '4000002500003155',
      exp_month: 11,
      exp_year: year,
      cvc: '314'
    }
  end

  def insufficient_funds_card(year: '2024')
    {
      number: '4000008260003178',
      exp_month: 11,
      exp_year: year,
      cvc: '314'
    }
  end

  def create_test_product
    Stripe::Product.create(
      { id: 'test_subscription_product',
        name: 'Test Subscription Product',
        type: 'service' },
      api_key: api.api_key
    )
  end

  def delete_test_product
    Stripe::Product.delete(@product.id, {}, api_key: api.api_key)
  end

  def create_test_plan
    Stripe::Plan.create({ amount: 5000, currency: 'eur', interval: 'month', product: @product.id }, api_key: api.api_key)
  end

  def delete_test_plan
    Stripe::Plan.delete(@plan.id, {}, api_key: api.api_key)
  end

  def cancel_subscription(subscription)
    Stripe::Subscription.delete(subscription.id, {}, api_key: api.api_key)
  end

  def delete_customer(customer)
    Stripe::Customer.delete(customer.id, {}, api_key: api.api_key)
  end
end
