# frozen_string_literal: true

require 'spec_helper'

describe DmSubscriptionsStripe::CreateInitialSubscriptionService, type: :service do
  setup_account

  let(:user) { create(:user) }
  let(:payment_method) { 'payment_token' }
  let(:plan) { create(:subscription_plan) }
  let(:service) { described_class.new(current_user: user, payment_method_token: payment_method, subscription_plan: plan) }

  before do
    allow_any_instance_of(DmSubscriptionsStripe::PaymentApiStripeSubscription).to receive(:api_key).and_return 'api key'
    allow_any_instance_of(DmSubscriptionsStripe::PaymentApiStripeSubscription).to receive(:public_key).and_return 'public key'
  end

  describe '#call' do
    context 'with no user' do
      let(:user) { nil }

      it 'returns nil' do
        expect(service.call).to be_nil
      end
    end

    context 'when valid user' do
      before do
        allow(user).to receive(:current_site_profile).and_return(double(stripe_customer_id: 'cus_abcd'))
        allow(plan).to receive(:stripe_plan_id).and_return('plan_abcd')
      end

      context 'when customer not already created' do
        it 'returns nil' do
          allow(user).to receive(:current_site_profile).and_return(double(stripe_customer_id: nil))

          expect(service.call).to be_nil
        end
      end

      context 'when subscription plan not associated with Stripe plan' do
        it 'raises an exception' do
          allow(plan).to receive(:stripe_plan_id).and_return(nil)

          expect { service.call }.to raise_error(described_class::NoStripeSubscriptionPlan)
        end
      end

      it 'creates a stripe subscription' do
        allow_any_instance_of(DmSubscriptionsStripe::PaymentApiStripeSubscription)
          .to receive(:create_subscription_to_plan).and_return(status: 'active')

        expect(service.call).to eq(status: 'active')
      end
    end
  end
end
