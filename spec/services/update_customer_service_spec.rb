# frozen_string_literal: true

require 'spec_helper'

describe DmSubscriptionsStripe::UpdateCustomerService, type: :service do
  setup_account

  let(:customer_id) { 'cust_test_id' }
  let(:service) { described_class.new(customer_id: customer_id, options: {}) }

  before do
    allow_any_instance_of(DmSubscriptionsStripe::PaymentApiStripeSubscription).to receive(:api_key).and_return 'api key'
    allow_any_instance_of(DmSubscriptionsStripe::PaymentApiStripeSubscription).to receive(:public_key).and_return 'public key'
  end

  describe '#call' do
    context 'with no customer id' do
      let(:customer_id) { nil }

      it 'returns nil' do
        expect(service.call).to be_nil
      end
    end

    context 'when valid customer id' do
      before do
        allow_any_instance_of(DmSubscriptionsStripe::PaymentApiStripeSubscription)
          .to receive(:update_customer).and_return double(id: 'cus_test_id', description: 'Description')
      end

      describe 'customer update fails' do
        it 'returns nil' do
          allow_any_instance_of(DmSubscriptionsStripe::PaymentApiStripeSubscription)
            .to receive(:update_customer).and_return nil

          expect(service.call).to be_nil
        end
      end
    end
  end
end
