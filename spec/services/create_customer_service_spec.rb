# frozen_string_literal: true

require 'spec_helper'

describe DmSubscriptionsStripe::CreateCustomerService, type: :service do
  setup_account

  let(:user) { create(:user) }
  let(:payment_method) { 'payment_token' }
  let(:service) { described_class.new(current_user: user, payment_method_token: payment_method) }

  before do
    allow_any_instance_of(DmSubscriptionsStripe::PaymentApiStripeSubscription).to receive(:api_key).and_return 'api key'
    allow_any_instance_of(DmSubscriptionsStripe::PaymentApiStripeSubscription).to receive(:public_key).and_return 'public key'
  end

  describe '#call' do
    context 'with no user' do
      let(:user) { nil }

      it 'returns nil' do
        expect(service.call).to be_nil
      end
    end

    context 'when valid user' do
      before do
        allow_any_instance_of(DmSubscriptionsStripe::PaymentApiStripeSubscription)
          .to receive(:create_customer).and_return double(id: 'cus_test_id')
      end

      describe 'customer creation fails' do
        it 'returns nil' do
          allow_any_instance_of(DmSubscriptionsStripe::PaymentApiStripeSubscription)
            .to receive(:create_customer).and_return nil

          expect(service.call).to be_nil
        end
      end

      it 'stores the customer id' do
        customer = service.call

        expect(customer.id).to eq 'cus_test_id'
        expect(user.reload.current_site_profile.stripe_customer_id).to eq 'cus_test_id'
      end
    end
  end
end
