# frozen_string_literal: true

require 'spec_helper'

describe DmSubscriptionsStripe::HandleWebhookService, type: :service do
  include_context 'setup stripe mocking'

  let(:service) { described_class.new(event) }

  %w[customer.created customer.updated invoice.created
     invoice.payment_succeeded invoice.payment_failed
     customer.subscription.created].each do |event_type|
    event_sym = event_type.gsub('.', '_').to_sym

    it "handles #{event_type}" do
      unless live_stripe_test?
        event = StripeMock.mock_webhook_event(event_type)

        expect_any_instance_of(DmSubscriptionsStripe::HandleWebhookService).to receive(event_sym)

        described_class.new(event).call
      end
    end
  end
end
