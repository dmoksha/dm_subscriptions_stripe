# frozen_string_literal: true

FactoryBot.define do
  factory :country, class: 'DmCore::Country' do
    id          { 223 }
    code        { 'US' }
    english_name  { 'United States' }
  end
end
