# frozen_string_literal: true

FactoryBot.define do
  factory(:subscription_plan) do
    price_cents         { 1000 }
    price_currency      { 'EUR' }
    name                { 'Basic' }
    slug                { 'basic' }
    setup_price_cents   { 0 }

    trait :advanced do
      name              { 'Advanced' }
      price_cents       { 10 }
      price_currency    { 'EUR' }
      slug              { 'advanced' }
    end

    trait :free do
      name              { 'Free' }
      price_cents       { 0 }
      price_currency    { 'EUR' }
      slug              { 'free' }
    end

    # after_build do |plan|
    #   plan.stub('row_order=').and_return true
    # end
  end
end
