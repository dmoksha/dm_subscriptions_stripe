# frozen_string_literal: true

FactoryBot.define do
  factory(:subscription_payment) do
    amount_cents      { 1000 }
    amount_currency   { 'EUR' }
  end
end
