# frozen_string_literal: true

FactoryBot.define do
  factory(:subscription) do
    amount_cents      { 1000 }
    amount_currency   { 'EUR' }
    card_number       { 'XXXX-XXXX-XXXX-1111' }
    card_expiration   { '05-2012' }
    billing_id        { 'foo' }
    next_renewal_at   { 1.day.ago.to_s(:db) }
    plan_changed_on   { 5.day.ago.to_s(:db) }
    subscription_plan

    trait :one do
    end
  end
end

# one:
#   subscriber: localhost (Account)
