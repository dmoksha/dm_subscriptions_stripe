# frozen_string_literal: true

FactoryBot.define do
  factory :account do
    # id              1
    domain                      { 'test.example.com' }
    account_prefix              { 'test' }
    preferred_default_currency  { 'EUR' }

    factory :stripe_subscription_account do
      after(:build) do |account|
        account.preferred_subscription_processor = 'stripe_subscription'
      end
    end
  end
end
