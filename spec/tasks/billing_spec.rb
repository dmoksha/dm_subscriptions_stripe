# frozen_string_literal: true

require 'rake_helper'

describe 'scheduled billing rake tasks', type: :rake_task do
  include ActiveMerchant::Billing

  setup_account

  before do
    Rake.application.rake_require 'tasks/billing'
  end

  describe 'charge due subscriptions' do
    # let(:user) { create :user }
    #
    # it 'makes account inactive when charge fails' do
    #   subscription = create(:subscription,
    #                  subscriber: user, state: 'active', next_renewal_at: 5.days.ago)
    #
    #   expect { run_rake_task('scheduled:run_billing') }.to change { subscription.reload.state }
    #   expect(subscription.state).to eq 'inactive'
    # end
  end
end
