# frozen_string_literal: true

$LOAD_PATH.push File.expand_path('lib', __dir__)

#--- gem's version:
require 'dm_subscriptions_stripe/version'

#--- Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.platform    = Gem::Platform::RUBY
  s.name        = 'dm_subscriptions_stripe'
  s.version     = DmSubscriptionsStripe::VERSION
  s.authors     = ['Brett Walker']
  s.email       = ['github@digitalmoksha.com']
  s.homepage    = 'https://gitlab.com/dmoksha/dm_subscriptions_stripe'
  s.summary     = 'Customized Subscription/Membership Engine'
  s.description = 'Customized Subscription/Membership Engine'
  s.required_ruby_version = '>= 2.7.0'

  s.metadata = {
    'bug_tracker_uri' => 'https://gitlab.com/dmoksha/dm_subscriptions_stripe/-/issues',
    'source_code_uri' => 'https://gitlab.com/dmoksha/dm_subscriptions_stripe'
  }

  s.files       = Dir['{app,config,db,lib}/**/*'] + ['MIT-LICENSE', 'Rakefile', 'README.md']
  s.test_files  = Dir['spec/**/*']

  s.add_dependency 'rails', '> 5.0'

  #--- don't forget to add 'require' statement in engine.rb for any dependencies
  s.add_dependency 'activemerchant', '~> 1.99'
  s.add_dependency 'offsite_payments', '~> 2.2'
  s.add_dependency 'stripe', '~> 5.7.0'

  #--- make sure the following gems are included in your app's Gemfile
  # gem 'dm_core', git: 'https://github.com/digitalmoksha/dm_core.git'
end
