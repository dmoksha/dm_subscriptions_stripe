# frozen_string_literal: true

ActiveMerchant::Billing::Base.mode = :test unless Rails.env.production?
