# frozen_string_literal: true

DmSubscriptionsStripe::Engine.routes.draw do
  scope ':locale' do
    namespace :admin do
      scope 'sub' do
        get   '/subscription_plans/financials',          controller: :subscription_plans, action: :financials
        patch '/subscription_plans/sort',                controller: :subscription_plans, action: :sort, as: :subscription_plan_sort
        resources :subscription_plans do
          resources :subscriptions do
            member do
              match 'make_payment', action: 'make_payment', as: 'make_payment', via: %i[get post patch]
            end
          end
        end
      end
    end

    get   '/signup_plan/:plan',                  controller: :subscriptions, action: :signup_plan, as: :signup_plan
    post  '/create_subscription_plan',           controller: :subscriptions, action: :create_subscription_plan, as: :create_subscription_plan
    post  '/subscriptions/create_customer_and_subscription', controller: :stripe_billing, action: :create_customer_and_subscription, as: :create_customer_and_subscription
    post  '/subscriptions/confirm_subscription', controller: :stripe_billing, action: :confirm_subscription, as: :confirm_subscription
    post  '/subscriptions/webhook',              controller: :stripe_billing, action: :webhook, as: :webhook
  end
end
